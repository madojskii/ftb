package net.footballarea.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import net.footballarea.model.Role;
import net.footballarea.model.User;
import net.footballarea.model.UserDetailsImpl;
import net.footballarea.repository.UserRepository;

public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        // returns the get(0) of the user list obtained from the db
        User user = userRepository.findByUsername(username);
        
        UserDetailsImpl userDetail=new UserDetailsImpl();
        userDetail.setUser(user);

        return userDetail;

    }

}