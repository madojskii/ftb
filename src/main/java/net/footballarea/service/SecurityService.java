package net.footballarea.service;

public interface SecurityService {
	
    String findLoggedInUsername();

}