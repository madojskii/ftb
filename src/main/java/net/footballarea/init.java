package net.footballarea;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.footballarea.model.Match;
import net.footballarea.model.Role;
import net.footballarea.model.Team;
import net.footballarea.model.User;
import net.footballarea.repository.MatchRepository;
import net.footballarea.repository.RoleRepository;
import net.footballarea.repository.TeamRepository;
import net.footballarea.repository.UserRepository;

@Component
public class init implements ApplicationListener<ContextRefreshedEvent> {
 
    boolean alreadySetup = false;
 
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private TeamRepository teamRepository;
    
    @Autowired
    private MatchRepository matchRepository;
    
    public init(){
    	super();
    }

    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup)
            return;
        else if(userRepository.findAll().isEmpty() == false){
        	return;
        }
       
        Role adminRole =  createRoleIfNotFound("ROLE_ADMIN");
        Role userRole =  createRoleIfNotFound("ROLE_USER");
        //Role sedziaRole =  createRoleIfNotFound("ROLE_SEDZIA");
        User user = new User();
        user.setName("Marcin");
        user.setSurname("Miniuk");
        user.setEmail("test@test.com");
        user.setBirthDate(new Date(1994,8,17));
        user.setPassword("1234");
        user.setUsername("madoy");
        user.setRole(userRole);
      
        
        
        User user1 = new User();
        user1.setName("Madoy");
        user1.setSurname("Nakamura");
        user1.setEmail("test@test.com");
        user1.setBirthDate(new Date(1994,8,17));
        user1.setPassword("1234");
        user1.setUsername("madoy1");
        user1.setRole(userRole);
        
 
        
        Team team1 = new Team("byczki", "bks", 0, 0, 0, 0, 1);
        teamRepository.save(team1);
        
        
        Team team2 = new Team("koksy", "kks", 1, 0, 0, 3, 2);
        teamRepository.save(team2);
        
        user1.setTeam(team1);
        userRepository.save(user1);
        
        user.setTeam(team2);
        userRepository.save(user);
        
        
        List<Team> teams1 = new ArrayList<>();
        teams1.add(team1);
        teams1.add(team2);
        
       
        User user2 = new User();
        user2.setName("test1");
        user2.setSurname("Nakamura");
        user2.setEmail("test@test.com");
        user2.setPassword("1234");
        user2.setUsername("madoy2");
        user2.setRole(userRole);
        
        User user3 = new User();
        user3.setName("test3");
        user3.setSurname("Nakamura");
        user3.setEmail("test@test.com");
        user3.setPassword("1234");
        user3.setUsername("madoy3");
        user3.setRole(adminRole);
        
        userRepository.save(user2);
        userRepository.save(user3);
        
 
        alreadySetup = true;
    }
 
    @Transactional
    private Role createRoleIfNotFound(String name) {
        Role role = roleRepository.findByRole(name);
        if (role == null) {
        	role = new Role(name);
        	roleRepository.save(role);
        }
        return role;
    }
}