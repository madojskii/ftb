package net.footballarea.model;

import java.util.Date;
import java.util.List;
import java.util.Collection;

import javax.transaction.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "game")
public class Match {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Transient
	private Team team1;
	@Transient
	private Team team2;
	private String host;
	private String status;
	@Column(name="post_status1")
	private String postStatus1 = "brak";
	@Column(name="post_status2")
	private String postStatus2 = "brak";
	private Date data;
	private String type;
	private String score = "-:-";
	@Transient
	private String team2Name;
	private int team1Goals;
	private int team2Goals;
	@ManyToMany
	private List<Team> teams;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "match")
	private List<Comment> comments;
	
	public Match(){
		
	}
	
	public Match(Date data,  String type, String score){
		this.data = data;
		this.type = type;
		this.score = score;
		this.status = "oczekujący";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Team getTeam1() {
		return team1;
	}

	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	public Team getTeam2() {
		return team2;
	}

	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
	
	public String getTeam2Name(){
		return team2Name;
	}
	
	public void setTeam2Name(String team2Name){
		this.team2Name = team2Name;
	}
	
	public List<Team> getTeams(){
		return teams;
	}
	
	public void setTeams(List<Team> teams){
		this.teams = teams;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public String getPostStatus1() {
		return postStatus1;
	}

	public void setPostStatus1(String postStatus1) {
		this.postStatus1 = postStatus1;
	}

	public String getPostStatus2() {
		return postStatus2;
	}

	public void setPostStatus2(String postStatus2) {
		this.postStatus2 = postStatus2;
	}

	public int getTeam1Goals() {
		return team1Goals;
	}

	public void setTeam1Goals(int team1Goals) {
		this.team1Goals = team1Goals;
	}

	public int getTeam2Goals() {
		return team2Goals;
	}

	public void setTeam2Goals(int team2Goals) {
		this.team2Goals = team2Goals;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
