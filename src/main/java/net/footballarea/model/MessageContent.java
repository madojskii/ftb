package net.footballarea.model;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "content")
public class MessageContent {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String content;
	@ManyToOne(fetch = FetchType.EAGER)
	private Message message;
	private Date data;
	@ManyToOne(fetch = FetchType.EAGER)
	private User user;

	public MessageContent() {
		this.data = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
	}

	public MessageContent(String content, Message message, User user) {
		this.content = content;
		this.message = message;
		this.data = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
