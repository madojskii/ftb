package net.footballarea.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String role;
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="role")
    private Collection<User> users;
	
	public Role(){
		
	}
	
	public Role(String role){
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public String getRole() {
		return role;
	}
	
	public Collection<User> getUsers(){
		return users;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setRole(String role) {
		this.role= role;
	}
	
	public void setUsers(Collection<User> users){
		this.users = users;
	}

}
