package net.footballarea.model;

import java.io.File;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import org.springframework.web.multipart.MultipartFile;



@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userID;
    private String name;
    private String surname;
    private String username;
    @Column(columnDefinition="Date")
    private Date birthDate;
    private String email;
    private String password;
    @Transient
    private String passwordConfirm;
    @Transient
    private String oldPassword;
    private String photo;
    private String type;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id")
    private Role role;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="teamID")
    private Team team;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private List<TeamInvitations> invitationsID;
    
    @ManyToMany(mappedBy = "users")
    private List<Message> messages;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private List<Comment> comments;
    
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="user")
    private List<MessageContent> contents;
    
    public User(String name, String surname, String username, Date birthDate, String email, String password, String photo, String type, Role role){
    this.name = name;
    this.surname = surname;
    this.username = username;
    this.birthDate = birthDate;
    this.email = email;
    this.password = password;
    this.photo = photo;
    this.type = type;
    this.role = role;
    }
    
    public User(){
    	
    }
    
    public User(String name, String surname){
    	this.name = name;
    	this.surname = surname;
    }
    

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<TeamInvitations> getInvitationsID() {
		return invitationsID;
	}

	public void setInvitationsID(List<TeamInvitations> invitationsID) {
		this.invitationsID = invitationsID;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<MessageContent> getContents() {
		return contents;
	}

	public void setContents(List<MessageContent> contents) {
		this.contents = contents;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	@Override
	public String toString(){
		return this.name + " " + this.surname;	
	}
}