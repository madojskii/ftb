package net.footballarea.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "team")
public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int teamID;
	private String name;
	private String shortName;
	private int win;
	private int draw;
	private int lose;
	private int points;
	private int captainID;
	private String photo;
	private boolean isActive = true;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "team")
	private List<User> membersID;

	@ManyToMany(mappedBy = "teams")
	private List<Match> matches;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "team")
	private List<TeamInvitations> invitationsID;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "team")
	private List<Trophy> trophies;
	
	public Team(String name, String shortName, int win, int draw, int lose, int points, int captainID) {
		this.name = name;
		this.shortName = shortName;
		this.win = win;
		this.draw = draw;
		this.lose = lose;
		this.points = points;
		this.captainID = captainID;
	}

	public Team() {

	}

	public int getTeamID() {
		return teamID;
	}

	public void setTeamID(int teamID) {
		this.teamID = teamID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLose() {
		return lose;
	}

	public void setLose(int lose) {
		this.lose = lose;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getCaptainID() {
		return captainID;
	}

	public void setCaptainID(int captainID) {
		this.captainID = captainID;
	}

	public List<User> getMembersID() {
		return membersID;
	}

	public void setMembersID(List<User> membersID) {
		this.membersID = membersID;
	}

	public List<Match> getMatches() {
		return matches;
	}

	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<TeamInvitations> getInvitationsID() {
		return invitationsID;
	}

	public void setInvitationsID(List<TeamInvitations> invitationsID) {
		this.invitationsID = invitationsID;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public List<Trophy> getTrophies() {
		return trophies;
	}

	public void setTrophies(List<Trophy> trophies) {
		this.trophies = trophies;
	}
	
	@Override
	public String toString() {
		return name;

	}
}