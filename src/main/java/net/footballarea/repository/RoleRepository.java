package net.footballarea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>{
		
	Role findByRole(String role);
	
}