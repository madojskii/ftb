package net.footballarea.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import net.footballarea.model.Message;
import net.footballarea.model.User;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer>{
		
	List<Message> findByUsersContaining(User user);
	
	Message findById(int id);
}