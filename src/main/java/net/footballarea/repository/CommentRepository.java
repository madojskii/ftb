package net.footballarea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Comment;
@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer>{


}