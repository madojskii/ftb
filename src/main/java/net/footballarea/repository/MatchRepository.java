package net.footballarea.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Match;
import net.footballarea.model.Team;

@Repository
public interface MatchRepository extends CrudRepository<Match, Integer>{
		
	List<Match> findByData(Date data);
	
	List<Match> findAll();
	
	List<Match> findByTeams(Team team);
	
	Match findById(int id);
	
	List<Match> findByStatus(String status);
	
	List<Match> findByStatusOrStatus(String status1, String status2);
	
	List<Match> findByStatusOrStatusOrderByDataDesc(String status1, String status2);
	
	List<Match> findByPostStatus1OrPostStatus2(String status1, String status2);
	
	List<Match> findByPostStatus1OrPostStatus2AndStatusNot(String status1, String status2, String status);
}