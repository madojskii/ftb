package net.footballarea.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Team;
import net.footballarea.model.TeamInvitations;
import net.footballarea.model.User;


@Repository
public interface TeamInvitationsRepository extends CrudRepository<TeamInvitations, Integer>{
		
	List<TeamInvitations> findAll();
	
	List<TeamInvitations> findByUserOrderByStatusAsc(User user);
	
	List<TeamInvitations> findByTeam(Team team);
	
	TeamInvitations findById(int id);
}