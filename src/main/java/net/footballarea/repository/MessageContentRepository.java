package net.footballarea.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Message;
import net.footballarea.model.MessageContent;
import net.footballarea.model.User;

@Repository
public interface MessageContentRepository extends CrudRepository<MessageContent, Integer>{
		
	List<MessageContent> findByMessageOrderByData(Message message);
	
	List<MessageContent> findByMessageOrderByDataDesc(Message message);
}