package net.footballarea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Team;
import net.footballarea.model.User;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer>{
	

	Team findByName(String name);
	
	Team findByNameAndShortName(String name, String shortName);
	
	Team findByCaptainID(int id);
	
	Team findByTeamID(int id);
	
	List<Team> findAll();
	
	List<Team> findAllByOrderByPointsDesc();
	
	List<Team> findByNameContainingOrShortNameContaining(String name, String shortName);

}