package net.footballarea.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.footballarea.model.Team;
import net.footballarea.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	List<User> findByNameContainingOrSurnameContaining(String name, String surname);
	
	User findByNameAndSurname(String name, String surname);
	
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username, String Password);
	
	User findByUserID(int id);
	
	List<User> findAll();
	
	List<User> findByTeam(Team team);
	
}