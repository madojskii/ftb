package net.footballarea.validator;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import net.footballarea.model.Team;
import net.footballarea.repository.TeamRepository;

@Component
public class TeamValidator implements Validator {
    @Autowired
    private TeamRepository teamRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return Team.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Team team = (Team) o;

       ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (team.getName().length() < 5 || team.getName().length() > 15) {
            errors.rejectValue("name", "Size.teamForm.name");
        }
        if (teamRepository.findByName(team.getName()) != null) {
            errors.rejectValue("name", "Duplicate.teamForm.name");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortName", "NotEmpty");
        if (team.getShortName().length() != 3) {
            errors.rejectValue("shortName", "Size.teamForm.shortName");
        }
    }
}