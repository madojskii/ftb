package net.footballarea.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.footballarea.model.Message;
import net.footballarea.model.MessageContent;
import net.footballarea.model.Team;
import net.footballarea.model.TeamInvitations;
import net.footballarea.model.User;
import net.footballarea.repository.MessageContentRepository;
import net.footballarea.repository.MessageRepository;
import net.footballarea.repository.TeamInvitationsRepository;
import net.footballarea.repository.TeamRepository;
import net.footballarea.repository.UserRepository;

@Controller
public class UserController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TeamInvitationsRepository teamInvitationsRepository;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	MessageContentRepository messageContentRepository;
	

	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
	public String userDetails(@PathVariable String username, Map<String, Object> model, Principal principal)
			throws ParseException {
		User user = userRepository.findByUsername(username);
		if(user == null){
			return "/error/nieTak";
		}
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		boolean isOwner = false;
		if (loggedUser.getUserID() == user.getUserID())
			isOwner = true;
		model.put("isOwner", isOwner);
		model.put("user", user);
		return "/user/Details";
	}

	@RequestMapping(value = "/user/edit", method = RequestMethod.GET)
	public String userEdit(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		List<String> typeList = new ArrayList<String>();
		typeList.add("Zawodnik");
		typeList.add("Sedzia");
		typeList.add("Obserwator");
		user.setPassword("");
		model.put("typeList", typeList);
		model.put("user", user);
		return "/user/Edit";
	}

	@RequestMapping(value = "/user/proccessUserEdit", method = RequestMethod.POST)
	public String proccessUserEdit(@ModelAttribute("user") User user, Map<String, Object> model, Principal principal)
			throws IOException, ParseException {
		System.out.println("EDYCJA TYPA WCHODZI");
		System.out.println(user.toString() + "   " + user.getName() + "   " + user.getBirthDate());
		String logged = ((Authentication) principal).getName();
		User userOld = userRepository.findByUsername(logged);
		userOld.setName(user.getName());
		userOld.setSurname(user.getSurname());
		userOld.setEmail(user.getEmail());
		userOld.setBirthDate(user.getBirthDate());
		userOld.setType(user.getType());
		userRepository.save(userOld);
		return "redirect:/user/"+userOld.getUsername();
	}

	@RequestMapping(value = "/user/avatar")
	public String avatar() {
		return "/user/avatar";
	}

	@RequestMapping(value = "/user/changeAvatar", method = RequestMethod.POST)
	public String changeAvatar(@RequestParam("file") MultipartFile file, Map<String, Object> model, Principal principal) {
		String fileName = null;
		String path = System.getProperty("user.dir")+"/target/classes/static/avatar/user/";
		path = path.replace("\\", "/");
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		if (!file.isEmpty()) {
			if(file.getSize() > 9000000){
				return "/user/avatar";
			}
			try {
				BufferedImage avatar = ImageIO.read(file.getInputStream());
				if (avatar.getHeight() > 300 || avatar.getWidth() > 300) {
					return "/user/avatar";
				} else {
					fileName = file.getOriginalFilename();
					File outputfile = new File(path + user.getUsername() + ".jpg");
					ImageIO.write(avatar, "jpg", outputfile);
					user.setPhoto(user.getUsername() + ".jpg");
					System.out.println(user.getPhoto() + " tak");
					userRepository.save(user);
					return "redirect:/user/"+user.getUsername();
				}
			} catch (Exception e) {
				return "/user/avatar";
			}
		} else {
			return "/user/avatar";
		}
	}

	@RequestMapping(value = "/user/Invitations", method = RequestMethod.GET)
	public String allInvitations(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		List<TeamInvitations> invitations = teamInvitationsRepository.findByUserOrderByStatusAsc(user);
		model.put("invitations", invitations);
		return "/user/allInv";
	}

	@RequestMapping(value = "/user/acceptInvitation/{id}", method = RequestMethod.POST)
	public String acceptInvitation(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		TeamInvitations invitation = teamInvitationsRepository.findById(id);
		Team team = invitation.getTeam();
		user.setTeam(team);
		userRepository.save(user);
		List<User> userList = team.getMembersID();
		userList.add(user);
		team.setMembersID(userList);
		teamRepository.save(team);
		invitation.setStatus("przyjęte");
		teamInvitationsRepository.save(invitation);
		return "/user/allInv";
	}

	@RequestMapping(value = "/user/rejectInvitation/{id}", method = RequestMethod.POST)
	public String rejectInvitation(@PathVariable int id, Map<String, Object> model, Principal principal) {
		TeamInvitations invitation = teamInvitationsRepository.findById(id);
		invitation.setStatus("odrzucone");
		teamInvitationsRepository.save(invitation);
		return "/user/allInv";
	}

	///////////////////////// MESSAGE////////////////////////
	@RequestMapping(value = "/user/message", method = RequestMethod.GET)
	public String allMessages(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		List<Message> messages = messageRepository.findByUsersContaining(user);
		for (Message message : messages) {
			List<User> userList = message.getUsers();
			if (userList.get(0).toString().equalsIgnoreCase(user.toString())) {
			//	message.setUser2(userList.get(1));
			} else {
				//message.setUser2(userList.get(0));
			}
			message.setData(messageContentRepository.findByMessageOrderByDataDesc(message).get(0).getData());
		}
		model.put("messages", messages);
		return "/user/Messages";
	}

	@RequestMapping(value = "/user/message/{message_id}", method = RequestMethod.GET)
	public String messageDetails(@PathVariable int message_id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Message message = messageRepository.findById(message_id);
		List<User> userList = message.getUsers();
		if (!userList.contains(user)) {
			return "/error/noMessage";
		}
//		if (userList.get(0).toString().equalsIgnoreCase(user.toString())) {
//			message.setUser2(userList.get(1));
//			message.setUser1(userList.get(0));
//		} else {
//			message.setUser2(userList.get(0));
//			message.setUser1(userList.get(1));
//		}
		
		List<MessageContent> messages = messageContentRepository.findByMessageOrderByData(message);
		model.put("message", message);
		model.put("messages", messages);
		MessageContent messageContent = new MessageContent();
		model.put("messageContent", messageContent);
		return "/user/MessageDetails";
	}
	
	@RequestMapping(value = "/user/message/new", method = RequestMethod.GET)
	public String newMessage(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Message message = new Message();
		model.put("message", message);
		List<User> users = userRepository.findAll();
		List<String> usersTS = new ArrayList<>();
		for(User user : users){
			if(loggedUser!=user){
				usersTS.add("\""+user.toString()+"\"");
			}
		}
		model.put("users", usersTS);
		return "/user/NewMessage";
	}
	
	@RequestMapping(value = "/user/message/new", method = RequestMethod.POST)
	public String newMessage(@ModelAttribute("message") Message message, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		MessageContent messageContent = new MessageContent(message.getContent(), message, user);
		System.out.println(messageContent.getData());
		
		List<MessageContent> userMS = user.getContents();
		userMS.add(messageContent);
		user.setContents(userMS);
		List<Message> messageList = user.getMessages();
		messageList.add(message);
		user.setMessages(messageList);
		
		List<MessageContent> messageMS = new ArrayList<>();
		messageMS.add(messageContent);
		message.setContents(messageMS);
		message.setHost(user.getName()+" "+user.getSurname());
		List<User> userList = new ArrayList<>();
		userList.add(user);
		String[] usersFromModel = message.getUser2().split(",");
		String name,surname;
		for(int i=0;i<usersFromModel.length-1;i++){
			System.out.println(usersFromModel[i] +" dzielenie");
			String[] nameAndSurname = usersFromModel[i].split(" ");
			if(i==0){
			name = nameAndSurname[0];
			surname = nameAndSurname[1];
			}else{
			name = nameAndSurname[1];
			surname = nameAndSurname[2];
			}
			System.out.println(name+" "+surname);
			User user2 = userRepository.findByNameAndSurname(name, surname);
			userList.add(user2);
		}
		Set tmp = new LinkedHashSet(userList);
		userList.clear();
		userList.addAll(tmp);
		System.out.println(userList+" koniec");
		message.setUsers(userList);
		
		messageRepository.save(message);
		messageContentRepository.save(messageContent);
		userRepository.save(user);
		return "redirect:/user/message/"+message.getId();
	}
	
	@RequestMapping(value = "/user/message/{messageId}/add", method = RequestMethod.POST)
	public String addMessage(@ModelAttribute("messageContent") MessageContent messageContent, @PathVariable int messageId, Map<String, Object> model, Principal principal) {
		System.out.println("tutaj?");
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Message message = messageRepository.findById(messageId);
		MessageContent messageContent1 = new MessageContent();
		messageContent1 = messageContent;
		messageContent1.setUser(user);
		messageContent1.setMessage(message);
		
		List<MessageContent> userMS = user.getContents();
		userMS.add(messageContent1);
		user.setContents(userMS);
		
		List<Message> messageList = user.getMessages();
		messageList.add(message);
		user.setMessages(messageList);
		
		List<MessageContent> messageMS = message.getContents();
		messageMS.add(messageContent1);
		message.setContents(messageMS);
		System.out.println("a moze tutaj?");
		//messageRepository.save(message);
		System.out.println("juz po?");
		messageContentRepository.save(messageContent1);
		System.out.println("hihi");
		userRepository.save(user);
		System.out.println("POST WYKONYWANY");
		return "redirect:/user/message/{messageId}";
	}
	
	@RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
	public String changePassword(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		user.setPassword("");
		model.put("user", user);
		return "/user/password";
	}
	
	@RequestMapping(value = "/user/changePasswordConfirm", method = RequestMethod.POST)
	public String changePasswordConfirm(@ModelAttribute("user") User user, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User userOld = userRepository.findByUsername(logged);
		if(!userOld.getPassword().equals(user.getOldPassword())){
			System.out.println("stare nie nowe "+user.getOldPassword() +" "+ userOld.getPassword());
			return "/user/password";
		}
		if(!user.getPassword().equals(user.getPasswordConfirm())){
			System.out.println("potwierdzenie nie nowe "+user.getPassword()+" "+user.getPasswordConfirm());
			return "/user/password";
		}
		userOld.setPassword(user.getPassword());
		userRepository.save(userOld);
		return "redirect:/user/"+userOld.getUsername();
	}

}