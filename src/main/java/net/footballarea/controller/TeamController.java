package net.footballarea.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import net.footballarea.model.Match;
import net.footballarea.model.Team;
import net.footballarea.model.TeamInvitations;
import net.footballarea.model.Trophy;
import net.footballarea.model.User;
import net.footballarea.repository.MatchRepository;
import net.footballarea.repository.TeamInvitationsRepository;
import net.footballarea.repository.TeamRepository;
import net.footballarea.repository.TrophyRepository;
import net.footballarea.repository.UserRepository;
import net.footballarea.validator.TeamValidator;

@Controller
public class TeamController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	TeamInvitationsRepository teamInvitationsRepository;

	@Autowired
	MatchRepository matchRepository;

	@Autowired
	TrophyRepository trophyRepositopry;

	@Autowired
	TeamValidator teamValidator;

	@RequestMapping(value = "/team/create", method = RequestMethod.GET)
	public String createTeam(Map<String, Object> model, Principal principal) {
		String name = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(name);
		if (user.getTeam() != null) {
			return "/error/HaveTeamAlready";
		}
		Team team = new Team();
		model.put("teamForm", team);
		return "/team/TeamCreate";
	}

	@RequestMapping(value = "/team/create", method = RequestMethod.POST)
	public String proccessCreate(@ModelAttribute("teamForm") Team team, BindingResult bindingResult,
			Map<String, Object> model, Principal principal) {
		teamValidator.validate(team, bindingResult);
		if (bindingResult.hasErrors()) {
			return "/team/TeamCreate";
		}
		String name = ((Authentication) principal).getName();
		User captain = userRepository.findByUsername(name);
		team.setCaptainID(captain.getUserID());
		teamRepository.save(team);
		captain.setTeam(team);
		userRepository.save(captain);
		System.out.println("name: " + team.getName());
		System.out.println("short: " + team.getShortName());
		System.out.println("captain: " + captain.getName() + " " + captain.getSurname());
		System.out.println("members: " + team.getMembersID());

		return "/team/CreateSuccess";
	}

	@RequestMapping(value = "/team/table", method = RequestMethod.GET)
	public String viewTable(Map<String, Object> model) {
		List<Team> team = teamRepository.findAllByOrderByPointsDesc();
		model.put("teamForm", team);
		return "/team/ShowTable";
	}

	@RequestMapping(value = "/team/details/{teamName}", method = RequestMethod.GET)
	public String viewTeam(@PathVariable String teamName, Map<String, Object> model, Principal principal) {
		Team team = teamRepository.findByName(teamName);
		if (team == null) {
			return "/error/nieTak";
		}
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		boolean isCaptain = false;
		boolean isMember = false;

		if (team.getCaptainID() != -1) {
			Collection<User> members = team.getMembersID();
			User captain = userRepository.findByUserID(team.getCaptainID());
			if (loggedUser.getUserID() == captain.getUserID()) {
				isCaptain = true;
				isMember = true;
			} else if (members.contains(loggedUser)) {
				isMember = true;
			}
			model.put("captain", captain);
			model.put("members", members);
		}
		model.put("team", team);
		model.put("isCaptain", isCaptain);
		model.put("isMember", isMember);
		// dodać inf, ze druzyna off + zmiana jsp
		return "/team/viewTeam";
	}

	@RequestMapping(value = "/team/home", method = RequestMethod.GET)
	public String viewTeamHome(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		if (loggedUser.getTeam() == null) {
			return "/error/NoTeam";
		}
		Team team = teamRepository.findByCaptainID(loggedUser.getUserID());
		boolean isCaptain = false;
		boolean isMember = false;
		if (team.getCaptainID() != -1) {
			Collection<User> members = team.getMembersID();
			User captain = userRepository.findByUserID(team.getCaptainID());
			if (loggedUser.getUserID() == captain.getUserID()) {
				isCaptain = true;
				isMember = true;
			} else if (members.contains(loggedUser)) {
				isMember = true;
			}
			model.put("captain", captain);
			model.put("members", members);
		}
		model.put("team", team);
		model.put("isCaptain", isCaptain);
		model.put("isMember", isMember);
		// dodać inf, ze druzyna off + zmiana jsp
		return "/team/viewTeam";
	}

	@RequestMapping(value = "/userSearch", method = RequestMethod.GET)
	@ResponseBody
	public List<String> search(HttpServletRequest request, Principal principal) {
		List<User> users = new ArrayList<>();
		List<String> usersTS = new ArrayList<>();
		if (request.getParameter("term").length() >= 3) {
			User loggedUser = userRepository.findByUsername(((Authentication) principal).getName());
			users = userRepository.findByNameContainingOrSurnameContaining(request.getParameter("term"),
					request.getParameter("term"));
			for (User user : users) {
				if (loggedUser != user) {
					usersTS.add(user.toString());
				}
			}
		}
		return usersTS;
	}

	@RequestMapping(value = "/team/add", method = RequestMethod.GET)
	public String addMember(Map<String, Object> model) {
		User userForm = new User();
		model.put("userForm", userForm);
		return "/team/addMember";
	}

	@RequestMapping(value = "/team/add", method = RequestMethod.POST)
	public String processAddMember(@ModelAttribute("userForm") User userForm, Map<String, Object> model,
			Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team team = loggedUser.getTeam();
		String[] nameAndSurname = userForm.getName().split(" ");
		String name = nameAndSurname[0];
		String surname = nameAndSurname[1];
		User user = userRepository.findByNameAndSurname(name, surname);
		TeamInvitations invitation = new TeamInvitations();
		invitation.setStatus("oczekujące");
		invitation.setTeam(team);
		invitation.setUser(user);
		teamInvitationsRepository.save(invitation);
		// user.setTeam(team);
		// userRepository.save(user);
		// List<User> userList = team.getMembersID();
		// userList.add(user);
		// team.setMembersID(userList);
		// teamRepository.save(team);
		return "redirect:/team/Invitations";
	}

	@RequestMapping(value = "/team/edit", method = RequestMethod.GET)
	public String edit(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team team = loggedUser.getTeam();
		if (loggedUser.getTeam() == null || team.getCaptainID() != loggedUser.getUserID()) {
			return "/error/NotCaptain";
		}
		System.out.println(team.getTeamID() + " teamID");
		model.put("team", team);
		return "/team/edit";
	}

	@RequestMapping(value = "/team/edit", method = RequestMethod.POST)
	public String editPOST(@ModelAttribute("team") Team team, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team teamOld = loggedUser.getTeam();
		teamOld.setName(team.getName());
		teamOld.setShortName(team.getShortName());
		teamRepository.save(teamOld);
		return "/team/viewTeam";

	}

	@RequestMapping(value = "/team/inactive", method = RequestMethod.POST)
	public String delete(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team team = loggedUser.getTeam();
		if (loggedUser.getTeam() == null || team.getCaptainID() != loggedUser.getUserID()) {
			return "/error/NotCaptain";
		}
		System.out.println(team.getTeamID() + " teamID");
		List<User> members = userRepository.findByTeam(team);
		team.setCaptainID(-1);
		loggedUser.setTeam(null);
		for (User user : members) {
			user.setTeam(null);
		}
		members.add(loggedUser);
		userRepository.save(members);
		return "/team/ShowTable";
		//////////////////////// TODO/////////////////////
		// DO USERA DODAC POPRZEDNIE DRUZYNY - ZROBIC MODEL RELACJA WIELE DO
		//////////////////////// WIELU(?)
	}

	@RequestMapping(value = "/team/leave", method = RequestMethod.GET)
	public String leave(Map<String, Object> model, Principal principal) {
		return "/team/LeaveTeamConfirm";
	}

	@RequestMapping(value = "/team/leaveConfirm", method = RequestMethod.POST)
	public String leaveConfirm(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team team = loggedUser.getTeam();
		loggedUser.setTeam(null);
		userRepository.save(loggedUser);
		List<User> members = userRepository.findByTeam(team);
		members.remove(loggedUser);
		teamRepository.save(team);
		return "redirect:/team/table";
	}

	@RequestMapping(value = "/team/avatar")
	public String avatar(Principal principal) {
		String logged = ((Authentication) principal).getName();
		User loggedUser = userRepository.findByUsername(logged);
		Team team = loggedUser.getTeam();
		if (loggedUser.getTeam() == null || team.getCaptainID() != loggedUser.getUserID()) {
			return "/error/NotCaptain";
		}
		return "/team/avatar";
	}

	@RequestMapping(value = "/team/changeAvatar", method = RequestMethod.POST)
	public String changeAvatar(@RequestParam("file") MultipartFile file, Principal principal) {
		String fileName = null;
		String path = System.getProperty("user.dir") + "/target/classes/static/avatar/team/";
		path = path.replace("\\", "/");
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByTeamID(user.getTeam().getTeamID());
		if (!file.isEmpty()) {
			if (file.getSize() > 9000000) {
				return "/team/avatar";
			}
			try {
				BufferedImage avatar = ImageIO.read(file.getInputStream());
				if (avatar.getHeight() > 300 || avatar.getWidth() > 300) {
					return "/team/avatar";
				} else {
					fileName = file.getOriginalFilename();
					File outputfile = new File(path + team.getName() + ".jpg");
					ImageIO.write(avatar, "jpg", outputfile);
					team.setPhoto(team.getName() + ".jpg");
					System.out.println(team.getPhoto() + " tak");
					teamRepository.save(team);
					return "redirect:/team/home";
				}
			} catch (Exception e) {
				return "/team/avatar";
			}
		} else {
			return "/team/avatar";
		}
	}

	/////////////// INVITATIONS//////////
	@RequestMapping(value = "/team/Invitations", method = RequestMethod.GET)
	public String teamInvitations(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		List<TeamInvitations> invitations = teamInvitationsRepository.findByTeam(team);
		model.put("invitations", invitations);
		return "/team/allInv";
	}

	@RequestMapping(value = "/team/cancelInvitation/{id}", method = RequestMethod.POST)
	public String cancelInvitation(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		teamInvitationsRepository.delete(id);
		return "/team/allInv";
	}

	//////////////// MATCH////////////
	@RequestMapping(value = "/team/matches", method = RequestMethod.GET)
	public String showTeamMatches(Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		boolean isCaptain = false;
		List<Match> matches = matchRepository.findByTeams(team);
		for (Match match : matches) {
			match.setTeam1(team);
			if (match.getTeams().get(0) == team) {
				match.setTeam2(match.getTeams().get(1));
			} else {
				match.setTeam2(match.getTeams().get(0));
			}
			System.out.println(match.getTeams() + " k " + match.getTeam1());
		}
		if (user.getUserID() == team.getCaptainID()) {
			isCaptain = true;
		}
		model.put("isCaptain", isCaptain);
		model.put("matches", matches);
		return "/team/ShowMatches";
	}

	@RequestMapping(value = "/team/cancelMatchInvitation/{id}", method = RequestMethod.POST)
	public String cancelMatchInvitations(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		matchRepository.delete(id);
		return "redirect:/team/matches";
	}

	@RequestMapping(value = "/team/acceptMatch/{id}", method = RequestMethod.POST)
	public String acceptMatch(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		// Team team = teamRepository.findByCaptainID(user.getUserID());
		Match match = matchRepository.findById(id);
		match.setStatus("zaakceptowany");
		matchRepository.save(match);
		return "redirect:/team/matches";
	}

	@RequestMapping(value = "/team/rejectMatch/{id}", method = RequestMethod.POST)
	public String rejectMatch(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		// Team team = teamRepository.findByCaptainID(user.getUserID());
		Match match = matchRepository.findById(id);
		match.setStatus("odrzucony");
		matchRepository.save(match);
		return "redirect:/team/matches";
	}

	@RequestMapping(value = "/team/cancelMatch/{id}", method = RequestMethod.POST)
	public String cancelMatch(@PathVariable int id, Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		// Team team = teamRepository.findByCaptainID(user.getUserID());
		Match match = matchRepository.findById(id);
		matchRepository.delete(match);
		return "redirect:/team/matches";
	}

	@RequestMapping(value = "/team/{teamName}/trophies", method = RequestMethod.GET)
	public String viewTrophy(@PathVariable String teamName, Map<String, Object> model) {
		Team team = teamRepository.findByName(teamName);
		List<Trophy> trophies = trophyRepositopry.findByTeam(team);
		model.put("trophies", trophies);
		return "/team/trophies";
	}

}