package net.footballarea.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.footballarea.model.User;
import net.footballarea.repository.UserRepository;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	
	@Autowired
	UserRepository userRepository;

	@RequestMapping(method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
       
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
        model.addAttribute("username", name);
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        System.out.println(name+" "+authorities);

        return "Login";
    }

    
    @RequestMapping(method = RequestMethod.POST)
    public String processLogin(@ModelAttribute("loginForm") User user,
            Map<String, Object> model) {
    	 System.out.println(user.getName()+" post");
    	
        return "redirect:/";
    }
}