package net.footballarea.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import net.footballarea.model.Comment;
import net.footballarea.model.Match;
import net.footballarea.model.Team;
import net.footballarea.model.Trophy;
import net.footballarea.model.User;
import net.footballarea.repository.CommentRepository;
import net.footballarea.repository.MatchRepository;
import net.footballarea.repository.TeamRepository;
import net.footballarea.repository.TrophyRepository;
import net.footballarea.repository.UserRepository;

@Controller
public class AdminController {

	@Autowired
	MatchRepository matchRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	TrophyRepository trophyRepository;

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminMain(Map<String, Object> model) {
		return "/admin/index";
	}

	@RequestMapping(value = "/admin/protests", method = RequestMethod.GET)
	public String showProtests(Map<String, Object> model) {
		List<Match> matchesBefore = matchRepository.findByPostStatus1OrPostStatus2("protest", "protest");
		List<Match> matches = new ArrayList<>();
		for(Match match:matchesBefore){
			if(!match.getStatus().equalsIgnoreCase("zamknięty"))
					matches.add(match);				
		}
		model.put("matches", matches);
		return "/admin/protests";
	}

	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
	public String showUsers(Map<String, Object> model) {
		List<User> users = userRepository.findAll();
		model.put("users", users);
		return "/admin/users";
	}

	@RequestMapping(value = "/admin/teams", method = RequestMethod.GET)
	public String showTeams(Map<String, Object> model) {
		List<Team> teams = teamRepository.findAll();
		model.put("teams", teams);
		return "/admin/teams";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/match/room/{match_id}/protestRoom/verdict", method = RequestMethod.GET)
	public String protestVerdict(@PathVariable int match_id, Map<String, Object> model) {
		Match match = matchRepository.findById(match_id);
		match.setTeam1Goals(0);
		match.setTeam2Goals(0);
		List<Team> teams = match.getTeams();
		if (teams.get(0).toString().equalsIgnoreCase(match.getHost())) {
			match.setTeam1(teams.get(0));
			match.setTeam2(teams.get(1));
		} else {
			match.setTeam1(teams.get(1));
			match.setTeam2(teams.get(0));
		}
		model.put("match", match);
		return "/admin/ProtestVerdict";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/match/room/{match_id}/protestRoom/verdict/close", method = RequestMethod.POST)
	public String closeProtest(@ModelAttribute("match") Match match, @PathVariable int match_id,
			Map<String, Object> model) {
		Match matchOLD = matchRepository.findById(match_id);
		matchOLD.setStatus("zamknięty");
		matchOLD.setScore(match.getTeam1Goals() + ":" + match.getTeam2Goals());
		matchOLD.setTeam1Goals(match.getTeam1Goals());
		matchOLD.setTeam2Goals(match.getTeam2Goals());
		if (matchOLD.getType().equalsIgnoreCase("Ligowy")) {
			Team teamOld1 = teamRepository.findByTeamID(match.getTeam1().getTeamID());
			Team teamOld2 = teamRepository.findByTeamID(match.getTeam2().getTeamID());
			if (match.getTeam1Goals() > match.getTeam2Goals()) {
				teamOld1.setWin(teamOld1.getWin() + 1);
				teamOld1.setPoints(teamOld1.getPoints() + 3);
				teamOld2.setLose(teamOld2.getLose() + 1);
				teamRepository.save(teamOld1);
				teamRepository.save(teamOld2);
			} else if (match.getTeam1Goals() < match.getTeam2Goals()) {
				teamOld2.setWin(teamOld2.getWin() + 1);
				teamOld2.setPoints(teamOld2.getPoints() + 3);
				teamOld1.setLose(teamOld1.getLose() + 1);
				teamRepository.save(teamOld1);
				teamRepository.save(teamOld2);
			} else {
				teamOld1.setDraw(teamOld1.getDraw() + 1);
				teamOld1.setPoints(teamOld1.getPoints() + 1);
				teamOld2.setDraw(teamOld2.getDraw() + 1);
				teamOld2.setPoints(teamOld2.getPoints() + 1);
				teamRepository.save(teamOld1);
				teamRepository.save(teamOld2);
			}
		}
		matchRepository.save(matchOLD);
		return "redirect:/match/room/{match_id}";
	}
	
	@RequestMapping(value = "/admin/trophy", method = RequestMethod.GET)
	public String addTrophyTeamList(Map<String, Object> model) {
		List<Team> team = teamRepository.findAllByOrderByPointsDesc();
		model.put("teamForm", team);
		return "/admin/TrophyTable";
	}
	
	@RequestMapping(value = "/admin/trophy/add/{teamName}", method = RequestMethod.GET)
	public String addTrophy(Map<String, Object> model) {
		Trophy trophy = new Trophy();
		model.put("trophy", trophy);
		return "/admin/AddTrophy";
	}
	
	@RequestMapping(value = "/admin/trophy/add/{teamName}", method = RequestMethod.POST)
	public String addTrophy(@ModelAttribute("trophy") Trophy trophy, @PathVariable String teamName, 
			Map<String, Object> model) {
		Team team = teamRepository.findByName(teamName);
		trophy.setTeam(team);
		trophyRepository.save(trophy);
		List<Trophy> trophyList = team.getTrophies();
		trophyList.add(trophy);
		team.setTrophies(trophyList);
		teamRepository.save(team);
		return "redirect:/admin/trophy";
	}

}
