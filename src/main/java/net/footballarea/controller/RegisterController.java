package net.footballarea.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import net.footballarea.model.User;
import net.footballarea.repository.RoleRepository;
import net.footballarea.repository.UserRepository;
import net.footballarea.service.SecurityService;
import net.footballarea.validator.RegisterValidator;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

	@Autowired
	UserRepository userRepository;

	@Autowired 
	RegisterValidator registerValidator; 

	@Autowired
	SecurityService securityService;

	@Autowired
	RoleRepository roleRepository;

	@RequestMapping(method = RequestMethod.GET)
	public String viewRegistration(Map<String, Object> model) {
		User user = new User();
		model.put("userForm", user);

		List<String> typeList = new ArrayList<String>();
		typeList.add("Zawodnik");
		typeList.add("Sędzia");
		typeList.add("Obserwator");
		model.put("typeList", typeList);

		return "Registration";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processRegistration(@ModelAttribute("userForm") User user, BindingResult bindingResult,
			Map<String, Object> model)  {
		registerValidator.validate(user, bindingResult);
		if (bindingResult.hasErrors()) {
			System.out.println("validate error");
			List<String> typeList = new ArrayList<String>();
			typeList.add("Zawodnik");
			typeList.add("Sędzia");
			typeList.add("Obserwator");
			model.put("typeList", typeList);

			return "/Registration";
		}
		user.setRole(roleRepository.findByRole("ROLE_USER"));
		userRepository.save(user);
		return "RegistrationSuccess";
	}
}