package net.footballarea.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.footballarea.model.Team;
import net.footballarea.repository.TeamRepository;

@Controller
public class MainController {
	
	@Autowired
	TeamRepository teamRepository;
	


	@RequestMapping(value = { "/home", "/", "/resources/demos/style.css"}, method = RequestMethod.GET)
	public String welcomePage(Map<String, Object> model) {
		List<Team> teams = teamRepository.findAll();
		model.put("teams", teams);
		 System.out.println("Working Directory = " +
	              System.getProperty("user.dir"));
		return "index";
	}
	
	@RequestMapping(value = { "/test" }, method = RequestMethod.GET)
	public String testPage(Model model) {
		return "test";
	}
	
	@RequestMapping(value = { "/LoggedIn" }, method = RequestMethod.GET)
	public String loggedIn(Model model) {
		return "LoggedIn";
	}
	
	@RequestMapping(value = { "/403" }, method = RequestMethod.GET)
	public String accessDenied(Model model) {
		return "403";
	}
}
