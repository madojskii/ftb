package net.footballarea.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.footballarea.model.Comment;
import net.footballarea.model.Match;
import net.footballarea.model.Team;
import net.footballarea.model.User;
import net.footballarea.repository.CommentRepository;
import net.footballarea.repository.MatchRepository;
import net.footballarea.repository.TeamRepository;
import net.footballarea.repository.UserRepository;

@Controller
public class MatchController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	MatchRepository matchRepository;
	
	@Autowired
	CommentRepository commentRepository;

	@RequestMapping(value = "/teamSearch", method = RequestMethod.GET)
	@ResponseBody
	public List<String> search(HttpServletRequest request, Principal principal){
		List<Team> teams = new ArrayList<>();
		List<String> teamsTS = new ArrayList<>();
		User loggedUser = userRepository.findByUsername(((Authentication) principal).getName());
		teams = teamRepository.findByNameContainingOrShortNameContaining(request.getParameter("term"),request.getParameter("term"));
		for(Team team : teams){
			if(loggedUser.getTeam()!=team){
				teamsTS.add(team.toString());
			}
		}
		return teamsTS;
	}
	
	@RequestMapping(value = "/match/create", method = RequestMethod.GET)
	public String matchCreate(Map<String, Object> model, Principal principal) {
		Match match = new Match();
		model.put("matchForm", match);
		List<String> typeList = new ArrayList<String>();
		typeList.add("Towarzyski");
		typeList.add("Ligowy");
		model.put("typeList", typeList);
		
		return "/match/MatchCreate";
	}

	@RequestMapping(value = "/match/create", method = RequestMethod.POST)
	public String processMatchCreate(@ModelAttribute("matchForm") Match match, BindingResult bindingResult,
			Map<String, Object> model, Principal principal) {
		String name = ((Authentication) principal).getName();
		User logged = userRepository.findByUsername(name);
		Team team1 = teamRepository.findByCaptainID(logged.getUserID());
		// match.setTeam1(team1);
		List<Team> teams = new ArrayList<>();
		teams.add(team1);
		teams.add(teamRepository.findByName(match.getTeam2Name()));
		match.setHost(team1.getName());
		match.setTeams(teams);
		match.setStatus("oczekujące");
		matchRepository.save(match);
		model.put("team1Name", team1.getName());
		return "redirect:/team/matches";
	}

	@RequestMapping(value = "/match/all", method = RequestMethod.GET)
	public String show(Map<String, Object> model, Principal principal) {
		List<Match> matches = matchRepository.findByStatusOrStatusOrderByDataDesc("zaakceptowany","zamknięty");
		model.put("matches", matches);
		return "/match/ShowMatches";
	}

	@RequestMapping(value = "/match/room/{id}", method = RequestMethod.GET)
	public String room(@PathVariable int id, Map<String, Object> model, Principal principal) {
		Match match = matchRepository.findById(id);
		if(match == null){
			return "/error/NoMatchRoom";
		}
		if (!match.getStatus().equalsIgnoreCase("zaakceptowany") && !match.getStatus().equalsIgnoreCase("zamknięty")) {
			return "/error/NoMatchRoom";
		}
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		boolean isCaptain = false;
		List<Team> teams = match.getTeams();
		if (teams.get(0).toString().equalsIgnoreCase(match.getHost())) {
			match.setTeam1(teams.get(0));
			match.setTeam2(teams.get(1));
		} else {
			match.setTeam1(teams.get(1));
			match.setTeam2(teams.get(0));
		}
		if (match.getTeam1() == team || match.getTeam2() == team) {
			isCaptain = true;
		}
		model.put("isCaptain", isCaptain);
		model.put("match", match);
		return "/match/MatchRoom";
	}

	@RequestMapping(value = "/match/room/{id}/score", method = RequestMethod.GET)
	public String insertScore(@PathVariable int id, Map<String, Object> model, Principal principal) {
		Match match = matchRepository.findById(id);
		if (!match.getStatus().equalsIgnoreCase("zaakceptowany")) {
			return "/error/NoMatchRoom";
		}
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		List<Team> teams = match.getTeams();
		if (teams.get(0).toString().equalsIgnoreCase(match.getHost())) {
			match.setTeam1(teams.get(0));
			match.setTeam2(teams.get(1));
		} else {
			match.setTeam1(teams.get(1));
			match.setTeam2(teams.get(0));
		}
		// System.out.println(match.getTeam1() + " 1 " + match.getTeam2() + " 2
		// " + team + " 3 " + match.getHost());
		if (match.getTeam1() != team && match.getTeam2() != team) {
			return "/error/NotCaptain";
		}
		model.put("match", match);
		return "/match/insertScore";
	}

	@RequestMapping(value = "/match/room/{id}/updateMatchScore", method = RequestMethod.POST)
	public String updateMatchScore(@ModelAttribute("match") Match match, @PathVariable int id,
			Map<String, Object> model, Principal principal) {
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		Team team = teamRepository.findByCaptainID(user.getUserID());
		Match matchOld = matchRepository.findById(id);
		// System.out.println(match.getTeam1() + " 1 " + match.getTeam2() + " 2
		// " + team);
		if (match.getPostStatus1().equalsIgnoreCase("zaakceptowany")
				|| match.getPostStatus2().equalsIgnoreCase("zaakceptowany")) {
			System.out.println("teraz");
			if (team == match.getTeam1()) {
				if (matchOld.getTeam1Goals() == match.getTeam1Goals()
						&& matchOld.getTeam2Goals() == match.getTeam2Goals()) {
					match.setPostStatus1("zaakceptowany");
				} else {
					match.setPostStatus1("protest");
				}
			} else {
				if (matchOld.getTeam1Goals() == match.getTeam1Goals()
						&& matchOld.getTeam2Goals() == match.getTeam2Goals()) {
					match.setPostStatus2("zaakceptowany");
				} else {
					match.setPostStatus2("protest");
				}
			}
			if (match.getPostStatus1().equalsIgnoreCase(match.getPostStatus2())
					&& match.getPostStatus1().equalsIgnoreCase("zaakceptowany")) {
				matchOld.setScore(match.getTeam1Goals() + ":" + match.getTeam2Goals());
				matchOld.setStatus("zamknięty");
				if (match.getType().equalsIgnoreCase("Ligowy")) {
					Team teamOld1 = teamRepository.findByTeamID(match.getTeam1().getTeamID());
					Team teamOld2 = teamRepository.findByTeamID(match.getTeam2().getTeamID());
					if (match.getTeam1Goals() > match.getTeam2Goals()) {
						teamOld1.setWin(teamOld1.getWin() + 1);
						teamOld1.setPoints(teamOld1.getPoints() + 3);
						teamOld2.setLose(teamOld2.getLose() + 1);
						teamRepository.save(teamOld1);
						teamRepository.save(teamOld2);
					} else if (match.getTeam1Goals() < match.getTeam2Goals()) {
						teamOld2.setWin(teamOld2.getWin() + 1);
						teamOld2.setPoints(teamOld2.getPoints() + 3);
						teamOld1.setLose(teamOld1.getLose() + 1);
						teamRepository.save(teamOld1);
						teamRepository.save(teamOld2);
					} else {
						teamOld1.setDraw(teamOld1.getDraw() + 1);
						teamOld1.setPoints(teamOld1.getPoints() + 1);
						teamOld2.setDraw(teamOld2.getDraw() + 1);
						teamOld2.setPoints(teamOld2.getPoints() + 1);
						teamRepository.save(teamOld1);
						teamRepository.save(teamOld2);
					}
				}
			}
		} else {
			System.out.println("tutaj");
			if (team == match.getTeam1()) {
				match.setPostStatus1("zaakceptowany");
			} else {
				match.setPostStatus2("zaakceptowany");
			}
		}
		//matchOld.setData(match.getData());
		matchOld.setHost(match.getHost());
		matchOld.setPostStatus1(match.getPostStatus1());
		matchOld.setPostStatus2(match.getPostStatus2());
		matchOld.setTeam1Goals(match.getTeam1Goals());
		matchOld.setTeam2Goals(match.getTeam2Goals());
		matchRepository.save(matchOld);
		return "redirect:/match/room/{id}";
	}

	@RequestMapping(value = "/match/room/{id}/protestRoom", method = RequestMethod.GET)
	public String protestRoom(@PathVariable int id, Map<String, Object> model, Principal principal) {
		Match match = matchRepository.findById(id);
		if(match == null){
			return "/error/NoMatchRoom";
		}
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		if (!match.getPostStatus1().equalsIgnoreCase("protest")
				&& !match.getPostStatus2().equalsIgnoreCase("protest")) {
			return "/error/NoProtestRoom";
		}
		Comment comment = new Comment();	
		boolean isCaptain = false;
		List<Team> teams = match.getTeams();
		if (teams.get(0).toString().equalsIgnoreCase(match.getHost())) {
			match.setTeam1(teams.get(0));
			match.setTeam2(teams.get(1));
		} else {
			match.setTeam1(teams.get(1));
			match.setTeam2(teams.get(0));
		}
		if (user.getUserID() == match.getTeam1().getCaptainID()
				|| user.getUserID() == match.getTeam2().getCaptainID()) {
			isCaptain = true;
		}
		System.out.println(comment.getId()+" id" + comment.getContent());
		List<Comment> comments = match.getComments();
		model.put("commentForm", comment);
		model.put("match", match);
		model.put("comments", comments);
		model.put("isCaptain", isCaptain);
		return "/match/ProtestRoom";
	}

	@RequestMapping(value = "/match/room/{match_id}/protestRoom/comment", method = RequestMethod.POST)
	public String addComment(@ModelAttribute("commentForm") Comment comment, @PathVariable int match_id, Map<String, Object> model,
			Principal principal) {
		Match match = matchRepository.findById(match_id);
		String logged = ((Authentication) principal).getName();
		User user = userRepository.findByUsername(logged);
		List<Comment> comments = match.getComments();
		System.out.println(comment.getId()+" id" + comment.getContent());
		comment.setMatch(match);
		comment.setData(new java.util.Date(Calendar.getInstance().getTimeInMillis()));
		comment.setUser(user);
		commentRepository.save(comment);
		System.out.println(comment.getContent()+"  "+comment.getData());
		comments.add(comment);
		match.setComments(comments);
		matchRepository.save(match);
		return "redirect:/match/room/{match_id}/protestRoom";
	}
}
