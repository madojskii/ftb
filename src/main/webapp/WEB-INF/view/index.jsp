<%@include file="/WEB-INF/layout.jsp"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<head><style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
  }
  </style></head>
<div class="container">

	<div class="row row-offcanvas row-offcanvas-right">

		<div class="col-xs-12 col-sm-9">
			<div class="jumbotron">
				<h1>Witaj!</h1>
				<p>Jesteś piłkarzem amatorem, ale nudzi Cię ciągłe granie z
					kumplami o przysłowiową pietruszkę? Dołącz do nas wraz z drużyną i
					zacznij rywalizować z innymi amatorskimi drużynami!
					
					
					
					TODO. 1)Jak zacząć 2)Strona główna - podpięcie avatarów do drużyn</p>
			</div>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
				<c:forEach var="team" items="${teams}" varStatus="loop">
					<c:choose>
						<c:when test="${loop.count == 1 }">
						<div class="item active">
						</c:when>
						<c:otherwise>
						<div class="item">
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when test="${team.photo == null}">
						<a href="/team/details/${team.name }"><img src="/avatar/defaultTeam.jpg" alt="${team.name}">
					</a>
							
						</c:when>
						<c:otherwise>
							<a href="/team/details/${team.name }"><img src="/zdjecia/team/${team.photo}" alt="${team.name}">
					</a>
						</c:otherwise>
					</c:choose>
						</div>
					</c:forEach>
					
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" role="button"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel" role="button"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<!--/.col-xs-12.col-sm-9-->

		<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
			<div class="list-group">
				<a href="#" class="list-group-item active">Menu</a> 
				<a href="/team/table" class="list-group-item">Tabela</a> 
				<a href="/team/create" class="list-group-item">Stwórz drużynę</a> 
				<a href="/match/all" class="list-group-item">Wyświetl wszystkie mecze</a> 
			</div>
		</div>
		<!--/.sidebar-offcanvas-->
	</div>
	<!--/row-->
	<hr>

	<footer>
		<p>© Football Arena</p>
	</footer>

</div>
<!--/.container-->