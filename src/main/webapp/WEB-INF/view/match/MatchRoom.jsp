<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<h2>${match.team1} vs ${match.team2}</h2>
		<table class="table">
			<thead>
				<tr>
					<th>Drużyna</th>
					<th>vs</th>
					<th>Drużyna</th>
					<th>Data</th>
					<th>Wynik</th>
					<th>Typ</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/team/details/${match.team1}">${match.team1}</a></td>
					<td>vs</td>
					<td><a href="/team/details/${match.team2}">${match.team2}</a></td>
					<td><fmt:formatDate type="both" value="${match.data}" /></td>
					<td>${match.score}</td>
					<td>${match.type}</td>
					<c:if test="${isCaptain == true}">
						<c:if test="${match.status eq 'zaakceptowany'}">
							<c:if test="${match.postStatus1 ne 'protest' && match.postStatus2 ne 'protest' }">
							<td><form action="/match/room/${match.id}/score"
									method="get">
									<div class="controls">
										<button class="btn btn-primary btn-sm">Wprowadź wynik</button>
									</div>
								</form></td>
								
								<td><form action="/match/room/${match.id}/score"
									method="get">
									<div class="controls">
										<button class="btn btn-primary btn-sm">Zgłoś protest</button>
									</div>
								</form></td>
								</c:if>
								<c:if test="${match.postStatus1 eq 'protest' || match.postStatus2 eq 'protest' }">
								<td><form action="/match/room/${match.id}/protestRoom"
									method="get">
									<div class="controls">
										<button class="btn btn-primary btn-sm">Sprawdź protest</button>
									</div></form></td>
								</c:if>
						</c:if>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>