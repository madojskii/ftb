<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>
    <div align="center">
        <table border="0">
            <tr>
                <td colspan="2" align="center">
                    <h3>Thank you for creating match!</h3>
                </td>
            </tr>
             <tr>
                <td>Ty:</td>
                <td>${team1Name}</td>
            </tr>
            <tr>
                <td>Przeciwnik:</td>
                <td>${matchForm.team2Name}</td>
            </tr>
            <tr>
                <td>data:</td>
                <td>${matchForm.data}</td>
            </tr>
            <tr>
                <td>wynik:</td>
                <td>${matchForm.score}</td>
            </tr>
            <tr>
                <td>typ:</td>
                <td>${matchForm.type}</td>
            </tr>
        </table>
    </div>
</body>
</html>