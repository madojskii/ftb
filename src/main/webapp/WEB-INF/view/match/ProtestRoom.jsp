<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<h2>${match.team1} vs ${match.team2}</h2>
		<table class="table">
			<thead>
				<tr>
					<th>Drużyna</th>
					<th>vs</th>
					<th>Drużyna</th>
					<th>Data</th>
					<th>Typ</th>
					<th>Zgłoszony przez</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/team/details/${match.team1}">${match.team1}</a></td>
					<td>vs</td>
					<td><a href="/team/details/${match.team2}">${match.team2}</a></td>
					<td><fmt:formatDate type="both" value="${match.data}" /></td>
					<td>${match.type}</td>
					<td><c:choose>
							<c:when test="${match.postStatus1 eq 'protest'}">
								<c:out value="${match.team1}"></c:out>
							</c:when>
							<c:otherwise>
								<c:out value="${match.team2}"></c:out>
							</c:otherwise>
						</c:choose></td>
					<td><sec:authorize var="admin" access="hasRole('ROLE_ADMIN')">
							<form action="protestRoom/verdict"
									method="get">
									<div class="controls">
										<button class="btn btn-primary btn-sm">Zamknij protest</button>
									</div>
								</form>
						</sec:authorize></td>
				</tr>
			</tbody>
		</table>
		<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">Komentarze</div>
				<div class="panel-body">
					<div class="container">

						<c:forEach items="${comments}" var="comments">
							<div class="row message-bubble">
								<p class="text-muted">${comments.user.name} ${comments.user.surname}(${comments.user.team.name}) data: <fmt:formatDate type="both" value="${comments.data}" /></p>
								<span>${comments.content }</span>
								
							</div>
							<br>
						</c:forEach>
					</div>
					<c:if test="${isCaptain == true || admin}">
					<form:form action="protestRoom/comment" modelAttribute="commentForm" method="post" >
					<div class="panel-footer">
						<div class="input-group">
							<form:input path="content" type="text" class="form-control"/> <span
								class="input-group-btn">
								<input type="submit" class="btn btn-default" type="button" value="Wyślij"/>
							</span>
						</div>
					</div>
					</form:form>
					</c:if>
				</div>
			</div>
		</div>
	</div>
		
	</div>
</body>
</html>