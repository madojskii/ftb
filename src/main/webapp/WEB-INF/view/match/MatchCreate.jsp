<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Match</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/jquery.datetimepicker.min.js"></script>
 <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
<script type="text/javascript">
$( function() {
	$( "#teams" ).autocomplete({
	      source: '${pageContext.request.contextPath}/teamSearch'
	    });
	});
</script>
<script type="text/javascript">
$( function() {
	$( "#datetimepicker2" ).datetimepicker({
		locale: 'pl'
	    });
	});
</script>
</head>
<body>
<div align="center">
        <form:form action="create" modelAttribute="matchForm" method="post" >
            <table border="0">
            <fieldset>
                <div id="legend">
					<legend class="">Stwórz mecz</legend>
				</div>
				<div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="team2Name">Przeciwnik:</label>
					<spring:bind path="name">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="team2Name" class="input-xlarge"
								placeholder="Nazwa lub skrót" autofocus="true" id="teams"></form:input>
								<br>
							<form:errors path="team2Name"></form:errors>
						</div>
						
					</spring:bind>
					<p class="help-block"></p>
					</div>
					
					<div class="control-group">
					<!-- Skrót -->
					<label class="control-label" for="data">Data:</label>
					<spring:bind path="shortName">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="data" path="data" class="input-xlarge"
								placeholder="" autofocus="true" id="datetimepicker2"></form:input>
								<br>
							<form:errors path="data"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
				</div>
				
				<div class="control-group">
					<!-- Skrót -->
					<label class="control-label" for="type">Typ:</label>
					<spring:bind path="shortName">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:select path="type" class="input-xlarge"
								placeholder="" autofocus="true" items="${typeList}"></form:select>
								<br>
							<form:errors path="type"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
				</div>
				</fieldset>
                   <div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Stwórz</button>
					</div>
				</div>
            </table>
        </form:form>
    </div>
</body>
</html>