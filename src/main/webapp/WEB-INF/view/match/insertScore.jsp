<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<h2>${match.team1} vs ${match.team2}</h2>
		<form:form action="updateMatchScore" modelAttribute="match" method="post"
			enctype="multipart/form-data">
			<table class="table">
				<thead>
					<tr>
						<th>Drużyna</th>
						<th>vs</th>
						<th>Drużyna</th>
						<th>Wynik</th>
						<th>Typ</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><form:hidden path="team1" />${match.team1}</td>
						<td><form:hidden path="postStatus1"/>vs<form:hidden path="postStatus2"/><form:hidden path="type"/></td>
						<td><form:hidden path="team2"/>${match.team2}</td>
						<td><form:input path="team1Goals"/>:<form:input path="team2Goals"/></td>
						<td><form:hidden path="host"/>${match.type}</td>
						<td><div class="controls">
						<button class="btn btn-success btn-sm">Potwierdź</button>
						</div></td>
					</tr>
				</tbody>
			</table>
		</form:form>
	</div>
</body>
</html>