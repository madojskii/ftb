<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<c:set value="1990-01-01" var="dateString" />
	<div align="center">
		<form:form class="form-horizontal" action="register"
			modelAttribute="userForm" method="POST">
			<fieldset>
				<div id="legend">
					<legend class="">Rejestracja</legend>
				</div>
				<div class="control-group">
					<!-- Username -->
					<label class="control-label" for="username">Username</label>
					<spring:bind path="username">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="username" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
								<br>
							<form:errors path="username"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block">Username powinien zawierać od 3 do 10 znaków</p>
				</div>

				<div class="control-group">
					<!-- Imie -->
					<label class="control-label" for="firstname">Imie</label>
					<spring:bind path="name">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="name" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
							<form:errors path="name"></form:errors>
						</div>
					</spring:bind>
				</div>

				<div class="control-group">
					<!-- Nazwisko -->
					<label class="control-label" for="surname">Nazwisko</label>
					<spring:bind path="surname">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="surname" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
							<form:errors path="surname"></form:errors>
						</div>
					</spring:bind>
				</div>

				<div class="control-group">
					<!-- E-mail -->
					<label class="control-label" for="email">E-mail</label>
					<spring:bind path="email"> 
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="email" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
							<form:errors path="email"></form:errors>
						</div>
						</spring:bind>
				</div>

				<div class="control-group">
					<!-- Hasło -->
					<label class="control-label" for="password">Hasło</label>
					<spring:bind path="password">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="password" path="password" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
								<br>
							<form:errors path="password"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block">Hasło powinno zawierać conajmniej 4 znaki</p>
				</div>

				<div class="control-group">
					<!-- Potwierdź hasło -->
					<label class="control-label" for="password_confirm">Potwierdź
						hasło</label>
					<spring:bind path="passwordConfirm">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="password" path="passwordConfirm"
								class="input-xlarge" placeholder="" autofocus="true"></form:input>
								<br>
							<form:errors path="passwordConfirm"></form:errors>
						</div>
					</spring:bind>
				</div>

				<div class="control-group">
					<!-- Urodziny -->
					<label class="control-label" for="birthDate">Data urodzin</label>
					<spring:bind path="birthDate">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="date" path="birthDate" class="input-xlarge"
								value="${dateString }" autofocus="true" id="birthDate"  ></form:input>
								<br>
							<form:errors path="birthDate"></form:errors>
						</div>
					</spring:bind>
				</div>

				<div class="form-group">
					<!-- Typ -->
					<label class="control-label" for="type">Typ</label>
					<spring:bind path="type">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:select type="text" path="type" class="input-xlarge"
								placeholder="" autofocus="true" items="${typeList}"></form:select>
								<br>
							<form:errors path="type"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block">Zawodnik/Sędzia/Obserwator</p>
				</div>
			</fieldset>
				<div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Zarejestruj</button>
					</div>
				</div>
		</form:form>
	</div>
</body>
</html>