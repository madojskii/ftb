<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

$( function() {
    var availableTags = ${users};
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#users" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  } );
</script>
</head>
<body>
    <div align="center">
        <form:form modelAttribute="message" method="post" >
            <table border="0">
            
                <div id="legend">
					<legend class="">Wyślij wiadomość</legend>
				</div>
                <div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="title">Tytuł</label>
					<spring:bind path="title">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:textarea type="text" path="title" class="input-xlarge"
								placeholder="" autofocus="true"></form:textarea>
								<br>
							<form:errors path="title"></form:errors>
						</div>	
					</spring:bind>
					<p class="help-block"></p>
					</div>
                
                <div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="users2">Odbiorca</label>
					<spring:bind path="user2">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:textarea type="text" path="user2" class="input-xlarge"
								placeholder="Wpisz imie lub nazwisko" id="users" autofocus="true"></form:textarea>
								<br>
							<form:errors path="user2"></form:errors>
						</div>	
					</spring:bind>
					<p class="help-block"></p>
					</div>
                 <div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="message">Wiadomość</label>
					<spring:bind path="content">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:textarea type="text" path="content" class="input-xlarge"
								placeholder="" autofocus="true" rows="3" cols="50" ></form:textarea>
								<br>
							<form:errors path="content"></form:errors>
						</div>	
					</spring:bind>
					<p class="help-block"></p>
					</div>

                <div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Wyślij</button>
					</div>
				</div>
            </table>
        </form:form>
    </div>
</body>
</html>