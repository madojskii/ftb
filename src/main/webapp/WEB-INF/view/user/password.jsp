<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
	<div align="center">
		<form:form class="form-horizontal" action="changePasswordConfirm"
			modelAttribute="user" method="POST">
			<fieldset>
				<div id="legend">
					<legend class="">Zmień hasło</legend>
				</div>

				<div class="control-group">
					<!-- Hasło -->
					<label class="control-label" for="oldPassword">Stare hasło</label>
					<spring:bind path="password">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="Password" path="oldPassword" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
							<form:errors path="oldPassword"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
				</div>


				<div class="control-group">
					<!-- Hasło -->
					<label class="control-label" for="password">Nowe hasło</label>
					<spring:bind path="password">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="password" path="password" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
							<form:errors path="password"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block">Hasło powinno zawierać conajmniej 4 znaki</p>
				</div>

				<div class="control-group">
					<!-- Potwierdź hasło -->
					<label class="control-label" for="password_confirm">Potwierdź
						hasło</label>
					<spring:bind path="passwordConfirm">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="password" path="passwordConfirm"
								class="input-xlarge" placeholder="" autofocus="true"></form:input>
							<form:errors path="passwordConfirm"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
				</div>
			</fieldset>
				<div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Zmień</button>
					</div>
				</div>
		</form:form>
	</div>
</body>
</html>