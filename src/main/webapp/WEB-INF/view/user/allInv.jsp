<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl_PL">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
	request.setCharacterEncoding("8859_2");
%>
<title>Registration Success</title>
</head>
<body>
	<div class="container" align="center">
		<table class="table">
				<h2>Lista zaproszeń</h2>
			<tr>
				<th>POZ.</th>
				<th>Od</th>
				<th>Status</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach var="inv" items="${invitations}" varStatus="loop">
				<tr>
					<td><c:out value="${loop.count}" /></td>
					<td><c:out value="${inv.team}" /></td>
					<td><c:out value="${inv.status}" /></td>
					<c:if test="${inv.status eq 'oczekujące'}">
						<td><form action="/user/acceptInvitation/${inv.id}"
								method="post">
								<button class="btn btn-success btn-sm">akceptuj</button>
							</form></td>
						<td><form action="/team/rejectInvitation/${inv.id}"
								method="post">
								<button class="btn btn-danger btn-sm">odrzuć</button>
							</form></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>