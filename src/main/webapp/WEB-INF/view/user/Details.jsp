<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>

	<div class="container-fluid well span6">
		<div class="container">
			<div class="span2">
				<c:choose>
					<c:when test="${user.photo == null}">
						<img src="/avatar/defaultUser.jpg" class="img-circle" />
					</c:when>
					<c:otherwise>
						<img src="/zdjecia/user/${user.photo}" class="img-circle" />
					</c:otherwise>
				</c:choose>
			</div>
			<div class="span8">
				<h3>${user.username}</h3>
				<h6>Imie: ${user.name}</h6>
				<h6>Nazwisko: ${user.surname}</h6>
				<h6>Data urodzin: ${user.birthDate}</h6>
				<h6>E-mail: ${user.email}</h6>
				<h6>Typ: ${user.type}</h6>
				<h6>Drużyna: ${user.team}</h6>
			</div>
			<c:if test="${isOwner == true}">
				<div class="span2">
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-info" data-toggle="dropdown"
							href="#"> Akcje <span class="icon-cog icon-white"></span><span
							class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="/user/edit"><span class="icon-wrench"></span>
									Edytuj</a></li>
							<li><a href="/user/avatar"><span class="icon-trash"></span>
									Zmień avatar</a></li>
							<li><a href="/user/changePassword"><span class="icon-trash"></span>
									Zmień hasło</a></li>
							<li><a href="/user/Invitations"><span class="icon-trash"></span>
									Sprawdź zaproszenia</a></li>
						</ul>
					</div>
				</div>
			</c:if>
		</div>
	</div>
</body>
</html>