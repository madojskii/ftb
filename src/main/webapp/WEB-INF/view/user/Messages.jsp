<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<h2>Twoje wiadomości</h2>
		<table class="table">
		<tr><td><a href="/user/message/new" class="btn btn-info btn-lg">
          <span class="glyphicon glyphicon-envelope"></span> Nowa wiadomość 
        </a></td></tr>
			<c:forEach items="${messages}" var="message">
				<tr>
					<td><a href="/user/message/${message.id}"> <c:out
								value="Temat: ${message.title} nadawca: ${message.host} data: "></c:out><fmt:formatDate type="both" 
            value="${message.data}" /></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>