<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">${message.title} Członkowie: <c:forEach items="${message.users }" var="user">
				<a href="/user/${user.username}">${user }</a> 
				</c:forEach>  </div>
				<div class="panel-body">
					<div class="container">

						<c:forEach items="${messages}" var="message">
							<div class="row message-bubble">
								<p class="text-muted">${message.user} <fmt:formatDate type="both" 
            value="${message.data}" /></p>
								<span>${message.content }</span>
								
							</div>
							<br>
						</c:forEach>
					</div>
					<form:form action="${message.id}/add" modelAttribute="messageContent" method="post" >
					<div class="panel-footer">
						<div class="input-group">
							<form:input path="content" type="text" class="form-control"/> <span
								class="input-group-btn">
								<input type="submit" class="btn btn-default" type="button" value="Wyślij"/>
							</span>
						</div>
					</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>