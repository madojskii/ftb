<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hello World!</title>
    </head>
    <body>
    <c:choose>
        <c:when test="${pageContext.request.userPrincipal.name != null}">
		<h2>
			Welcome : ${pageContext.request.userPrincipal.name} |
		</h2>
	</c:when>
	<c:otherwise>
		Nikt??
	</c:otherwise>
	</c:choose>
        <form action="/logout" method="post">
            <input type="submit" value="Sign Out"/>

        </form>
    </body>
</html>