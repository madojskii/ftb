<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl_PL">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>
	<div class="container" align="center">
		<table class="table" border="0" cellpadding="10">
			<tr>
				<h1>Lista protestów</h1>
			</tr>
			<tr>
				<th>NR</th>
				<th>Drużyna</th>
				<th>vs</th>
				<th>Drużyna</th>
				<th>Data</th>
				<th>Typ</th>
				<th>Pokój protestu</th>
			</tr>
			<c:forEach var="match" items="${matches}" varStatus="loop">
				<tr>
					<td><c:out value="${loop.count}" /></td>
					<td><a href="/team/details/${match.teams.get(0)}">
						<c:out value="${match.teams.get(0)}" /></a></td>
					<td><c:out value="  vs  " /></td>
					<td><a href="/team/details/${match.teams.get(1)}">
						<c:out value="${match.teams.get(1)}" /></a></td>
					<td><fmt:formatDate type="both" value="${match.data}" /></td>
					<td><c:out value="${match.type} ${match.status }" /></td>
					<td><form action="/match/room/${match.id}/protestRoom"
								method="get">
								<button class="btn btn-primary btn-sm">Przejdź do protestu</button>
							</form></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>