<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>
	<div class="container" align="center">
		<table class="table table-bordered">
			<h2>Dodaj trofeum</h2>
			<tr>
				<th style="text-align:center">POZYCJA</th>
				<th width="400">NAZWA</th>
				<th style="text-align:center">ZWYCIĘSTWA</th>
				<th style="text-align:center">REMISY</th>
				<th style="text-align:center">PORAŻKI</th>
				<th style="text-align:center">PUNKTY</th>
			</tr>
			<c:forEach var="team" items="${teamForm}" varStatus="loop">
				<tr>
					<td align="center"><c:out value="${loop.count}" /></td>
					<td><a href="/team/details/${team.name}"><c:out
								value="${team.name}" /></a></td>
					<td align="center"><c:out value="${team.win}" /></td>
					<td align="center"><c:out value="${team.draw}" /></td>
					<td align="center"><c:out value="${team.lose}" /></td>
					<td align="center"><c:out value="${team.points}" /></td>
					<td><a href="/admin/trophy/add/${team.name}" class="btn btn-info btn-sm" role="button" >Dodaj trofeum</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>

