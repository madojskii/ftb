<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
</head>
<body>
    <div align="center">
        <form:form modelAttribute="trophy" method="post" >
            <table border="0">
            
                <div id="legend">
					<legend class="">Nadaj trofeum</legend>
				</div>
                <div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="name">Nazwa osiągnięcia</label>
					<spring:bind path="title">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:textarea type="text" path="name" class="input-xlarge"
								placeholder="" autofocus="true"></form:textarea>
								<br>
							<form:errors path="name"></form:errors>
						</div>	
					</spring:bind>
					<p class="help-block"></p>
					</div>
                <div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Zatwierdź</button>
					</div>
				</div>
            </table>
        </form:form>
    </div>
</body>
</html>