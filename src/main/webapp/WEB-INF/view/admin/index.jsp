<%@include file="/WEB-INF/layout.jsp"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<head></head>
<div class="container">

	<div class="row row-offcanvas row-offcanvas-right">

		<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
			<div class="list-group">
				<a href="#" class="list-group-item active">Menu</a> 
				<a href="#"	class="list-group-item">Wyświetl użytkowników</a> 
				<a href="#" class="list-group-item">Wyświetl drużyny</a> 
				<a href="/admin/trophy" class="list-group-item">Dodaj trofeum</a> 
				<a href="/admin/protests" class="list-group-item">Wyświetl protesty</a> 
			</div>
		</div>
		<!--/.sidebar-offcanvas-->
	</div>
	<!--/row-->
	<hr>

	<footer>
		<p>© Football Arena</p>
	</footer>

</div>
<!--/.container-->