<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Registration Success</title>
</head>
<body>
	<div class="container">
		<h2>${match.team1} vs ${match.team2}</h2>
		<form:form method="POST" modelAttribute="match" action="verdict/close">
		<table class="table">
			<thead>
				<tr>
					<th>Drużyna</th>
					<th>vs</th>
					<th>Drużyna</th>
					<th style="text-align:center">Wynik</th>
					<th>Data</th>
					<th>Typ</th>
					<th>Zgłoszony przez</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><form:hidden path="team1" /><a href="/team/details/${match.team1}">${match.team1}</a></td>
					<td>vs</td>
					<td><form:hidden path="team2" /><a href="/team/details/${match.team2}">${match.team2}</a></td>
					<td align="center"><form:input size="1" path="team1Goals"/>:<form:input size="1" path="team2Goals"/></td>
					<td><fmt:formatDate type="both" value="${match.data}" /></td>
					<td>${match.type}</td>
					<td><c:choose>
							<c:when test="${match.postStatus1 eq 'protest'}">
								<c:out value="${match.team1}"></c:out>
							</c:when>
							<c:otherwise>
								<c:out value="${match.team2}"></c:out>
							</c:otherwise>
						</c:choose></td>
						<td><form action="verdict/close" method="post">
								<button class="btn btn-success btn-sm">Potwierdź</button>
							</form></td>
	
				</tr>
			</tbody>
		</table>
	</form:form>
	</div>
</body>
</html>