<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>.nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  top: 0px;
  width: 300px;
  height: 100%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
  #main {
  	width:calc(100% - 300px);
  	float: right;
  }
}

body {
  margin: 0px;
  padding: 0px;
}</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>
<div class="nav-side-menu">
    <div class="brand">Brand Logo</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    <div class="menu-list">
        <ul id="menu-content" class="menu-content collapse out">
            <li>
                    <i class="fa fa-dashboard fa-lg" ></i> Panel drużyny              
            </li>
                <c:if test="${isMember == true }">    
                	<c:if test="${isCaptain == true}">       
            <li>
                <a href="/team/add">
                    <i class="fa fa-user fa-lg"></i> Dodaj zawodnika
                </a>
            </li>
            <li>
                <a href="/team/edit">
                    <i class="fa fa-users fa-lg"></i> Edytuj drużynę
                </a>
            </li>
            <li>
                <a href="/team/avatar">
                    <i class="fa fa-users fa-lg"></i> Zmień herb
                </a>
            </li>
            <li>
                <a href="/match/create">
                    <i class="fa fa-users fa-lg"></i> Stwórz mecz
                </a>
            </li>
            </c:if>
             <li>
                <a href="/team/Invitations">
                    <i class="fa fa-users fa-lg"></i> Lista zaproszonych osób
                </a>
            </li>
             <li>
                <a href="/team/matches">
                    <i class="fa fa-users fa-lg"></i> Lista oczekujących spotkań
                </a>
            </li>
            <li>
                <a href="/team/leave">
                    <i class="fa fa-users fa-lg"></i> Opuść drużynę
                </a>
            </li>
            </c:if>  
            <li>
                <a href="/team/${team.name}/trophies">
                    <i class="fa fa-users fa-lg"></i> Trofea
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container" id="main">
    <div class="row">
    <br>
    <br>
    <br>
        <div class="col-md-12">
            <div class="container-fluid well span6">

		
		
			<div class="span2">
				<c:choose>
						<c:when test="${team.photo == null}">
							<img src="/avatar/defaultTeam.jpg"  class="img-circle"/>
						</c:when>
						<c:otherwise>
							<img src="/zdjecia/team/${team.photo}"  class="img-circle"/>
						</c:otherwise>
					</c:choose>
			</div>
			<div class="span8">
				<h3>${team.name}</h3>
				<h6>Skrót: ${team.shortName}</h6>
				<h6>Kapitan: ${captain.name} ${captain.surname}</h6>
				<h6>Zwycięstwa: ${team.win}</h6>
				<h6>Remisy: ${team.draw}</h6>
				<h6>Porażki: ${team.lose}</h6>
				<h6>Punkty: ${team.points}</h6>
				<h6>Lista zawodników: <c:forEach var="members" items="${members}" varStatus="loop">
				
					<td><c:out value="${members.name}" /></td>
					<td><c:out value="${members.surname}" /></td>
				
			</c:forEach></h6>
			</div>

		
	</div>
        </div>
    </div>
</div>
</body>
</html>