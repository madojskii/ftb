<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl_PL">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>

	<div class="container" align="center">
		<table class="table">
			<tr>
					<h2>Czy na pewno chcesz opuścić drużynę?</h2>
			</tr>

				<tr>
							<form action="/team/leaveConfirm"
									method="post">
									<div class="controls">
						<button class="btn btn-success">Tak</button>
					</div>
								</form>
							<form action="/team/home"
									method="get">
									<div class="controls">
						<button class="btn btn-danger">Nie</button>
					</div>
								</form>
						
				</tr>
		</table>
	</div>
</body>
</html>