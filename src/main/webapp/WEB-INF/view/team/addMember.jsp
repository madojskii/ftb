<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Match</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$( function() {
	$( "#users" ).autocomplete({
	      source: '${pageContext.request.contextPath}/userSearch'
	    });
	});
</script>
</head>
<body>
	<div align="center">
		<form:form action="add" modelAttribute="userForm" method="post">
				<div id="legend">
					<legend class="">Dodaj zawodnika</legend>
				</div>
				<div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="name">Imie i Nazwisko</label>
					<spring:bind path="name">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="name" id="users" class="input-xlarge"
								placeholder="Wpisz imię lub nazwisko" autofocus="true"></form:input>
							<form:errors path="name"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
					</div>
				<div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Dodaj</button>
					</div>
				</div>
		</form:form>
	</div>
</body>
</html>