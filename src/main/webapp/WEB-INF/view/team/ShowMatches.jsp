<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl_PL">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>

	<div class="container" align="center">
		<table class="table">
			<tr>
					<h1>Wszystkie mecze</h1>
			</tr>
			<tr>
				<th>NR</th>
				<th>Przeciwnik</th>
				<th>Data</th>
				<th>Typ</th>
				<th>Status</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach var="match" items="${matches}" varStatus="loop">
				<tr>
					<td><c:out value="${loop.count}" /></td>
					<td><a href="/team/details/${match.team2}"><c:out value="${match.team2}" /></a></td>
					<td><fmt:formatDate type="both" value="${match.data}" /></td>
					<td><c:out value="${match.type}" /></td>
					<td><c:out value="${match.status}"></c:out></td>
					<c:if test="${isCaptain == true }">
						<c:if
							test="${match.host != match.team1 && match.status eq 'oczekujące'}">
							<td><form action="/team/acceptMatch/${match.id}"
									method="post">
									<div class="controls">
						<button class="btn btn-success btn-sm">akceptuj</button>
					</div>
								</form></td>
							<td><form action="/team/rejectMatch/${match.id}"
									method="post">
									<div class="controls">
						<button class="btn btn-danger btn-sm">odrzuć</button>
					</div>
								</form></td>
						</c:if>
						<c:if
							test="${match.status eq 'oczekujące' && match.host == match.team1}">
							<td><form action="/team/cancelMatch/${match.id}"
									method="post">
									<div class="controls">
						<button class="btn btn-danger btn-sm">anuluj</button>
					</div>
								</form></td>
						</c:if>
					</c:if>
					<c:if
							test="${match.status eq 'zaakceptowany'}">
							<td><form action="/match/room/${match.id}"
									method="get">
									<div class="controls">
						<button class="btn btn-primary btn-sm">Pokój meczu</button>
					</div>
								</form></td>
						</c:if>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>