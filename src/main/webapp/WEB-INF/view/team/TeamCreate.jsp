<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%> 
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div align="center">
        <form:form action="create" modelAttribute="teamForm" method="post" >
            <table border="0">
            <fieldset>
                <div id="legend">
					<legend class="">Stwórz drużynę</legend>
				</div>
				<div class="control-group">
					<!-- Nazwa -->
					<label class="control-label" for="name">Nazwa</label>
					<spring:bind path="name">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="name" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
								<br>
							<form:errors path="name"></form:errors>
						</div>
						
					</spring:bind>
					<p class="help-block"></p>
					</div>
					
					<div class="control-group">
					<!-- Skrót -->
					<label class="control-label" for="shortName">Skrót</label>
					<spring:bind path="shortName">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="shortName" class="input-xlarge"
								placeholder="" autofocus="true"></form:input>
								<br>
							<form:errors path="shortName"></form:errors>
						</div>
					</spring:bind>
					<p class="help-block"></p>
				</div>
				</fieldset>
                   <div class="control-group">
					<!-- Button -->
					<div class="controls">
						<button class="btn btn-success">Zatwierdź</button>
					</div>
				</div>
            </table>
        </form:form>
    </div>
</body>
</html>