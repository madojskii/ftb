<%@include file="/WEB-INF/layout.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Zaloguj się</h3>
					</div>
					<div class="panel-body">
						<form accept-charset="UTF-8" role="form" action="/login"
							method="POST">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="Username"
										name="username" type="text">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Hasło"
										name="password" type="password" value="">
								</div>
								<input class="btn btn-lg btn-success btn-block" type="submit"
									value="Zaloguj">
							</fieldset>
						</form>
						<c:if test="${param.error ne null}">
							<div>Nieprawidłowy username lub hasło.</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>