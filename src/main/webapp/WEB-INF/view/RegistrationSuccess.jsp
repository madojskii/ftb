<%@include file="/WEB-INF/layout.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
%>
<%@ page language="java" pageEncoding="utf-8"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Success</title>
</head>
<body>
    <div align="center">
        <table border="0">
            <tr>
                <td colspan="2" align="center"><h2>Rejestracja zakończona sukcesem!</h2></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <h3>Możesz się teraz <a href="/login">zalogować!</a></h3>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>